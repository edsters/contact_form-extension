<?php namespace Emange\ContactFormExtension;

use Anomaly\FormsModule\Form\Contract\FormInterface;
use Anomaly\FormsModule\Form\Handler\Contract\FormHandlerExtensionInterface;
use Anomaly\Streams\Platform\Addon\Extension\Extension;
use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Emange\ContactFormExtension\Handler\Command\GetContactFormBuilder;

/**
 * Class ContactFormExtension
 *
 * @link          http://anomaly.is/streams-platform
 * @author        AnomalyLabs, Inc. <hello@anomaly.is>
 * @author        Ryan Thompson <ryan@anomaly.is>
 * @package       Emange\ContactFormExtension
 */
class ContactFormExtension extends Extension implements FormHandlerExtensionInterface
{

    /**
     * This extension provides the contact
     * form handler for the Forms module.
     *
     * @var string
     */
    protected $provides = 'anomaly.module.forms::form_handler.contact';

    /**
     * Return the form's builder instance.
     *
     * @param FormInterface $form
     * @return FormBuilder
     */
    public function builder(FormInterface $form)
    {
        return $this->dispatch(new GetContactFormBuilder($form));
    }

    /**
     * Integrate the form handler's services
     * with the primary form's builder instance.
     *
     * @param FormBuilder $builder
     */
    public function integrate(FormBuilder $builder)
    {
        $fields = $builder->getFields();
        $fields['user_email_field'] = ['required' => TRUE];
        $builder->setFields($fields);
        $sections = $builder->getSections();

        foreach ($sections[0]['tabs'] as &$tab) {
            $field_key = array_search('user_email_field', $tab['fields']);

            if ($field_key) {
                array_forget($tab['fields'], $field_key);
            }

            if ($tab['title'] == 'module::tab.form'){
                $tab['fields'] = array_prepend($tab['fields'],'user_email_field');
            }
        }

        $builder->setSections($sections);

        return $builder;
    }
}

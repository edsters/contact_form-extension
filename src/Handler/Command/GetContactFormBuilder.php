<?php namespace Emange\ContactFormExtension\Handler\Command;

use Anomaly\FormsModule\Form\Contract\FormInterface;
use Anomaly\FormsModule\Form\FormAutoresponder;
use Emange\ContactFormExtension\Form\FormMailer;
use Emange\ContactFormExtension\Handler\ContactFormBuilder;
use Illuminate\Routing\Redirector;

/**
 * Class GetContactFormBuilder
 *
 * @link          http://anomaly.is/streams-platform
 * @author        AnomalyLabs, Inc. <hello@anomaly.is>
 * @author        Ryan Thompson <ryan@anomaly.is>
 * @package       Emange\ContactFormExtension\Handler\Command
 */
class GetContactFormBuilder
{

    /**
     * The form instance.
     *
     * @var FormInterface
     */
    protected $form;

    /**
     * Create a new GetContactFormBuilder instance.
     *
     * @param FormInterface $form
     */
    public function __construct(FormInterface $form)
    {
        $this->form = $form;
    }

    /**
     * Handle the command.
     *
     * @param ContactFormBuilder $builder
     * @return ContactFormBuilder
     */
    public function handle(ContactFormBuilder $builder, Redirector $redirect)
    {
        $stream = $this->form->getFormEntriesStream();

        $builder->on(
            'saved',
            function (FormMailer $mailer, FormAutoresponder $autoresponder) use ($builder) {
                $mailer->send($this->form, $builder);
                $autoresponder->send($this->form, $builder);
            }
        );

        return $builder
            ->setActions(['submit'])
            ->setModel($stream->getEntryModel())
            ->setOption('panel_class', 'section')
            ->setOption('enable_defaults', FALSE)
            ->setOption('success_message', $this->form->getSuccessMessage() ?: FALSE)
            ->setOption('redirect', $this->form->getSuccessRedirect() ?: $redirect->back());
    }
}
